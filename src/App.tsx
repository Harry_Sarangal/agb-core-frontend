import React, { useEffect } from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { createMuiTheme, CssBaseline, ThemeProvider } from '@material-ui/core'
import { SnackbarProvider } from 'notistack'
import * as colors from '@material-ui/core/colors'
import Login from './components/pages/Login/Login'
import Layout from './components/layout/Layout'
import Home from './components/pages/Home/Home'
import Profile from './components/pages/Profile/Profile'
import Prescriptions from './components/pages/Prescriptions/Prescriptions'
import ProtectedRoute from './components/layout/ProtectedRoute'
import useStore from './core/state'
// import NotFound from './components/pages/NotFound/NotFound'

const theme = createMuiTheme({
  palette: {
    primary: colors.blue,
    secondary: colors.blueGrey,
  },
})

function App() {
  const { getAuthUser, loading } = useStore((state) => state)

  useEffect(() => {
    getAuthUser()
  }, [getAuthUser])

  if (loading) return null

  return (
    <ThemeProvider theme={theme}>
      <SnackbarProvider
        maxSnack={3}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
      >
        <CssBaseline />
        <Router>
          <div>
            <Switch>
              <Route exact path="/login">
                <Login />
              </Route>
              <Layout>
                <ProtectedRoute exact path="/">
                  <Home />
                </ProtectedRoute>
                <ProtectedRoute exact path="/profile">
                  <Profile />
                </ProtectedRoute>
                <ProtectedRoute exact path="/user/:id/prescriptions">
                  <Prescriptions />
                </ProtectedRoute>
                {/* TODO: fix later */}
                {/* <Route>
                  <NotFound />
                </Route> */}
              </Layout>
            </Switch>
          </div>
        </Router>
      </SnackbarProvider>
    </ThemeProvider>
  )
}

export default App
