import React from 'react'
import {
  AppBar,
  Button,
  Grid,
  Toolbar,
  Typography,
  Link,
  Container,
  Box,
} from '@material-ui/core'
import { NavLink } from 'react-router-dom'
import Copyright from '../shared/Copyright'
import { useStyles } from './styles'
import { footers } from './utils'
import Logo from '../shared/Logo'
import useStore from '../../core/state'
import { Roles } from '../../core/types'

interface LayoutProps {
  children: React.ReactNode
}

export default function Layout({ children }: LayoutProps) {
  const classes = useStyles()
  const { logout, user } = useStore((state) => state)

  const isPatient = user?.roleId === 4

  return (
    <div className={classes.root}>
      <AppBar
        position="static"
        color="default"
        elevation={0}
        className={classes.appBar}
      >
        <Toolbar className={classes.toolbar}>
          <Logo />
          <nav className={classes.navbar}>
            {!isPatient && (
              <Link
                variant="button"
                color="textPrimary"
                className={classes.link}
                component={NavLink}
                to="/"
                exact
                activeClassName={classes.linkActive}
              >
                Management
              </Link>
            )}
            <Link
              variant="button"
              color="textPrimary"
              className={classes.link}
              component={NavLink}
              to="/profile"
              exact
              activeClassName={classes.linkActive}
            >
              Profile
            </Link>
          </nav>
          <Link
            variant="subtitle1"
            color="textPrimary"
            component={NavLink}
            to="/profile"
            className={classes.linkName}
          >
            {user?.name} ({Roles[(user?.roleId as number) - 1]})
          </Link>
          <Button
            color="primary"
            variant="outlined"
            onClick={() => logout()}
            className={classes.link}
          >
            Logout
          </Button>
        </Toolbar>
      </AppBar>

      {children}

      <Container maxWidth="md" component="footer" className={classes.footer}>
        <Grid container spacing={4} justify="space-evenly">
          {footers.map((footer) => (
            <Grid item xs={6} sm={3} key={footer.title}>
              <Typography variant="h6" color="textPrimary" gutterBottom>
                {footer.title}
              </Typography>
              <ul>
                {footer.description.map((item) => (
                  <li key={item}>
                    <Link href="#" variant="subtitle1" color="textSecondary">
                      {item}
                    </Link>
                  </li>
                ))}
              </ul>
            </Grid>
          ))}
        </Grid>
        <Box mt={5}>
          <Copyright />
        </Box>
      </Container>
    </div>
  )
}
