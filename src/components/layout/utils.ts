export const footers = [
  {
    title: 'Company',
    description: ['Team', 'History', 'Contact us', 'Locations'],
  },
  {
    title: 'Features',
    description: ['Find a doctor', 'Refer a patient', 'View clinical trials'],
  },
  {
    title: 'Resources',
    description: [
      'Newsletters',
      'Health Topics',
      'COVID-19 Treatment Guidelines',
    ],
  },
  {
    title: 'Legal',
    description: ['Privacy policy', 'Terms of use'],
  },
]
