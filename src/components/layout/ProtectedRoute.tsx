import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import { getSessionId } from '../../core/session'
import useStore from '../../core/state'

function PrivateRoute({ children, ...rest }: any) {
  const user = useStore((state) => state.user)
  return (
    <Route
      {...rest}
      render={({ location }) =>
        getSessionId() && user ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: location },
            }}
          />
        )
      }
    />
  )
}

export default PrivateRoute
