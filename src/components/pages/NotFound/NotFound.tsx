import React from 'react'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'

const useStyles = makeStyles((theme) => ({
  heroContent: {
    padding: theme.spacing(8, 0, 6),
  },
  message: {
    margin: '3em 0',
  },
}))

export default function NotFound() {
  const classes = useStyles()

  return (
    <Container maxWidth="md" component="main" className={classes.heroContent}>
      <Typography
        variant="h4"
        align="center"
        color="textSecondary"
        component="p"
        gutterBottom
        className={classes.message}
      >
        Sorry, page not found
      </Typography>
    </Container>
  )
}
