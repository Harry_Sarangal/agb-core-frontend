import React, { useEffect, useState } from 'react'
import { Theme, createStyles, makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import { Box, Container } from '@material-ui/core'
import { useParams } from 'react-router-dom'
import { useSnackbar, VariantType } from 'notistack'
import useStore from '../../../core/state'
import Prescription from '../../shared/Prescription'
import {
  Prescription as PrescriptionType,
  PrescriptionModalType,
  User,
} from '../../../core/types'
import CreatePrescription from './CreatePrescriptionButton'
import Modal from './PrescriptionModal'
import PrescriptionDetailsModal from './Details'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    heroContent: {
      padding: theme.spacing(8, 0, 6),
    },
    root: {
      width: '100%',
      marginTop: '20px',
    },
    pageHeader: {
      borderBottom: 'solid 1px rgba(120, 144, 156, 0.5)',
      paddingBottom: '10px',
    },
    container: {
      flexWrap: 'wrap',
      alignItems: 'stretch',
    },
    error: {
      margin: '4em',
    },
    placeholder: {
      margin: '4em auto',
    },
  })
)

export default function Prescriptions() {
  const classes = useStyles()
  const { id } = useParams<{ id?: string | undefined }>()
  const [currentUser, setCurrentUser] = useState<User | null>()
  const {
    users,
    fetchUser,
    addPrescription,
    updatePrescription,
    user: loggedUser,
  } = useStore((state) => state)
  const user = users.find((u) => u.id === Number(id))
  const [isOpenDetails, setIsOpenDetails] = useState<boolean>(false)
  const [activeDetails, setActiveDetails] = useState<PrescriptionType | null>(
    null
  )

  const [isOpen, setIsOpen] = useState<PrescriptionModalType>(null)
  const [activePrescription, setActivePrescription] = useState<
    PrescriptionType
  >()
  const [error, setError] = useState<boolean>(false)
  const { enqueueSnackbar } = useSnackbar()

  useEffect(() => {
    setActiveDetails(
      currentUser?.prescriptions.find((p) => p.id === activeDetails?.id) || null
    )
  }, [currentUser, activeDetails?.id])

  function handleClose() {
    setIsOpen(null)
    setActivePrescription(undefined)
    setError(false)
  }

  useEffect(() => {
    ;(async function fetch() {
      setCurrentUser(user || (await fetchUser(Number(id))))
    })()
  }, [id, fetchUser, user])

  const handleClickVariant = (variant: VariantType, message: string) => {
    // variant could be success, error, warning, info, or default
    enqueueSnackbar(message, {
      variant,
    })
  }

  function handeAddPrescription(params: any) {
    addPrescription({ ...params, patientId: Number(id) }).then(async (v) => {
      if (v) {
        setCurrentUser(await fetchUser(Number(id)))
        handleClose()
        handleClickVariant('success', 'Success!')
      } else {
        setError(true)
      }
    })
  }

  function handeEditPrescription(prescriptionId: number, params: any) {
    updatePrescription(prescriptionId, params).then(async (v) => {
      if (v) {
        setCurrentUser(await fetchUser(Number(id)))
        handleClose()
        handleClickVariant('success', 'Prescription has been updated')
      } else {
        setError(true)
      }
    })
  }

  if (!currentUser) {
    return (
      <Typography
        variant="h6"
        align="center"
        color="textSecondary"
        component="p"
        className={classes.error}
      >
        User not found
      </Typography>
    )
  }

  const isDoctor = loggedUser?.roleId === 2

  return (
    <Container maxWidth="md" component="main" className={classes.heroContent}>
      <Box
        display="flex"
        alignItems="center"
        justifyContent="space-between"
        className={classes.pageHeader}
      >
        <Typography
          variant="h6"
          align="left"
          color="textPrimary"
          component="p"
          gutterBottom
        >
          {`${currentUser?.name}'s prescriptions`}
        </Typography>
      </Box>
      <div className={classes.root}>
        <Box
          display="flex"
          alignItems="center"
          justifyContent="even"
          className={classes.container}
        >
          {currentUser?.prescriptions.map((prescription) => (
            <Prescription
              prescription={prescription}
              key={prescription.id}
              isPrintable={isDoctor}
              handleViewClick={() => {
                setIsOpenDetails(true)
                setActiveDetails(prescription)
              }}
              handleOnClick={() => {
                if (isDoctor) {
                  setIsOpen('updatePrescription')
                  setActivePrescription(prescription)
                }
              }}
            />
          ))}
          {!currentUser?.prescriptions.length && !isDoctor && (
            <Typography
              color="textSecondary"
              variant="h5"
              className={classes.placeholder}
            >
              User has no prescriptions
            </Typography>
          )}
          {isDoctor && (
            <CreatePrescription
              handleCreatePrescrition={() => setIsOpen('addPrescription')}
            />
          )}
        </Box>
      </div>
      {isOpen && (
        <Modal
          isOpen={isOpen}
          handleClose={handleClose}
          error={error}
          addPrescription={handeAddPrescription}
          editPrescription={handeEditPrescription}
          prescription={activePrescription}
          user={currentUser}
        />
      )}
      <PrescriptionDetailsModal
        isOpen={isOpenDetails}
        handleClose={() => {
          setIsOpenDetails(true)
          setActiveDetails(null)
        }}
        prescription={activeDetails}
        user={currentUser}
        updateUser={setCurrentUser}
        handleClickVariant={handleClickVariant}
      />
    </Container>
  )
}
