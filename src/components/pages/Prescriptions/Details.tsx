import React, { useState } from 'react'
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import { Card, Link, makeStyles, Typography } from '@material-ui/core'
import { VariantType } from 'notistack'
import Prescription from '../../shared/Prescription'
import { Prescription as PrescriptionType, User } from '../../../core/types'
import AddCommentModal from './AddComment'
import useStore from '../../../core/state'

interface PrescriptionDetailsModalProps {
  isOpen: boolean
  prescription: PrescriptionType | null
  handleClose: () => void
  user: User
  updateUser: (user: User | null) => void
  handleClickVariant: (variant: VariantType, message: string) => void
}

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3, 4, 3, 4),
    '& .MuiCard-root': {
      margin: 0,
      marginBottom: '2em',
    },
  },
  actions: {
    padding: '18px 24px 8px',
  },
  select: {
    margin: '15px 0',
  },
  switch: {
    marginRight: 'auto',
  },
  comment: {
    padding: '1em',
  },
  placeholder: {
    margin: '3em 0',
  },
  addComment: {
    marginLeft: 'auto',
    display: 'block',
    marginTop: '1em',
    cursor: 'pointer',
  },
}))

export default function PrescriptionDetailsModal({
  isOpen,
  prescription,
  user,
  handleClose,
  updateUser,
  handleClickVariant,
}: PrescriptionDetailsModalProps) {
  const classes = useStyles()
  const [isCreateModalOpen, setIsCreateModalOpen] = useState<boolean>(false)
  const { updatePrescription, fetchUser } = useStore((state) => state)
  const [error, setError] = useState<boolean>(false)

  if (!prescription) return null

  const comments = prescription?.comments || []

  function handeEditPrescription(prescriptionId: number, params: any) {
    updatePrescription(prescriptionId, params).then(async (v) => {
      if (v) {
        updateUser(await fetchUser(Number(user.id)))
        setIsCreateModalOpen(false)
        handleClickVariant('success', 'Prescription has been updated')
      } else {
        setError(true)
      }
    })
  }

  return (
    <Dialog
      open={Boolean(isOpen)}
      onClose={handleClose}
      aria-labelledby="form-dialog-title"
    >
      <div className={classes.root}>
        <DialogTitle id="form-dialog-title">Prescription details</DialogTitle>
        <DialogContent>
          <Prescription prescription={prescription} />
          <DialogContentText>Comments:</DialogContentText>
          {comments.map((comment, id) => (
            <Card key={`comment${id + 1}`} className={classes.comment}>
              {comment}
            </Card>
          ))}
          {!comments.length && (
            <Typography
              color="textSecondary"
              align="center"
              className={classes.placeholder}
            >
              No comments available
            </Typography>
          )}
          <Link
            variant="subtitle1"
            color="primary"
            align="right"
            onClick={() => setIsCreateModalOpen(true)}
            className={classes.addComment}
          >
            Add a comment
          </Link>
        </DialogContent>
      </div>
      <AddCommentModal
        prescription={prescription}
        isOpen={isCreateModalOpen}
        handleClose={() => setIsCreateModalOpen(false)}
        editPrescription={handeEditPrescription}
        error={error}
      />
    </Dialog>
  )
}
