import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import AddIcon from '@material-ui/icons/Add'

const useStyles = makeStyles({
  root: {
    minWidth: 285,
    maxWidth: 285,
    margin: 10,
    opacity: 0.35,
    cursor: 'pointer',
    '&:hover': {
      opacity: 1,
      '& p': {
        opacity: 1,
      },
    },
  },
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    height: '100%',
    padding: '1.75em',
  },
  message: {
    fontSize: '1.25em',
    opacity: 0,
    position: 'absolute',
  },
  icon: {
    margin: 'auto',
    fontSize: '4em',
  },
})

interface CreatePrescriptionProps {
  handleCreatePrescrition: () => void
}

export default function CreatePrescription({
  handleCreatePrescrition,
}: CreatePrescriptionProps) {
  const classes = useStyles()

  return (
    <Card
      className={classes.root}
      variant="outlined"
      onClick={handleCreatePrescrition}
    >
      <CardContent className={classes.container}>
        <AddIcon className={classes.icon} color="secondary" />
      </CardContent>
    </Card>
  )
}
