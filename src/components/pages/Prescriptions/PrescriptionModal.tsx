import React, { useEffect, useRef, useState } from 'react'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import { makeStyles, Typography } from '@material-ui/core'
import Alert from '@material-ui/lab/Alert'
import { Prescription, PrescriptionModalType, User } from '../../../core/types'

interface PrescriptionModalProps {
  isOpen: PrescriptionModalType
  // eslint-disable-next-line react/require-default-props
  prescription?: Prescription
  handleClose: () => void
  addPrescription: (params: any) => void
  editPrescription: (id: number, params: any) => void
  error: boolean
  user: User
}

const labels = {
  addPrescription: {
    title: 'Add prescription',
    description:
      'Fill out the form and click submit to add a new prescription.',
  },
  updatePrescription: {
    title: 'Update prescription',
    description: 'You can update the prescription.',
  },
}

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3, 4, 3, 4),
  },
  actions: {
    padding: '18px 24px 8px',
  },
  select: {
    margin: '15px 0',
  },
  switch: {
    marginRight: 'auto',
  },
}))

export default function Modal({
  isOpen,
  handleClose,
  error,
  addPrescription,
  editPrescription,
  prescription,
  user,
}: PrescriptionModalProps) {
  const classes = useStyles()
  const [name, setName] = useState<string | undefined>(prescription?.name)
  const [refils, setRefils] = useState<string | undefined>(
    prescription ? String(prescription?.refils) : undefined
  )
  const [info, setInfo] = useState<string | undefined>(
    prescription?.information
  )

  const prevState = useRef<string | null>()
  useEffect(() => {
    if (prevState.current !== isOpen && isOpen === 'addPrescription') {
      setName(undefined)
      setRefils(undefined)
      setInfo(undefined)
    }
    prevState.current = isOpen
  }, [isOpen])

  const label: { title: string; description: string } = isOpen
    ? labels[isOpen]
    : { title: '', description: '' }

  function handleSubmit() {
    switch (isOpen) {
      case 'addPrescription': {
        addPrescription({
          name,
          refils: Number(refils),
          information: info,
        })
        break
      }
      case 'updatePrescription': {
        editPrescription(prescription?.id as number, {
          name,
          refils: Number(refils),
          information: info,
        })
        break
      }
      default:
    }
  }

  const isCreationMode = isOpen === 'addPrescription'

  const isWarning = user?.allergies?.find(
    (a) => a.toLocaleLowerCase() === name?.toLocaleLowerCase()
  )

  return (
    <Dialog
      open={Boolean(isOpen)}
      onClose={handleClose}
      aria-labelledby="form-dialog-title"
    >
      <div className={classes.root}>
        <DialogTitle id="form-dialog-title">{label.title}</DialogTitle>
        <DialogContent>
          <DialogContentText>{label.description}</DialogContentText>{' '}
          {isWarning && (
            <Alert severity="error">
              Warning - The patient has intolerance to this medicine!
            </Alert>
          )}
          <TextField
            margin="dense"
            id="name"
            label="Name"
            fullWidth
            autoFocus={Boolean(isCreationMode)}
            value={name}
            onChange={(e) => setName((e.target.value as unknown) as string)}
          />
          <TextField
            margin="dense"
            id="reils"
            label="Refils"
            value={refils}
            onChange={(e) => setRefils((e.target.value as unknown) as string)}
            fullWidth
          />
          <TextField
            margin="dense"
            id="address"
            label="Information"
            fullWidth
            value={info}
            onChange={(e) => setInfo((e.target.value as unknown) as string)}
          />
        </DialogContent>
        <DialogActions className={classes.actions}>
          {error && <Typography color="error">Something went wrong</Typography>}
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSubmit} color="primary">
            Submit
          </Button>
        </DialogActions>
      </div>
    </Dialog>
  )
}
