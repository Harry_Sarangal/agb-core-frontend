import React, { useState } from 'react'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import { makeStyles, Typography } from '@material-ui/core'
import { Prescription } from '../../../core/types'

interface AddCommentModalPrpos {
  isOpen: boolean
  // eslint-disable-next-line react/require-default-props
  prescription?: Prescription
  handleClose: () => void
  // addPrescription: (params: any) => void
  editPrescription: (id: number, params: any) => void
  error: boolean
}

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3, 4, 3, 4),
  },
  actions: {
    padding: '18px 24px 8px',
  },
  select: {
    margin: '15px 0',
  },
  switch: {
    marginRight: 'auto',
  },
}))

export default function AddCommentModal({
  isOpen,
  handleClose,
  error,
  editPrescription,
  prescription,
}: AddCommentModalPrpos) {
  const classes = useStyles()
  const [comment, setComment] = useState<string | undefined>()

  function handleSubmit() {
    if (comment) {
      editPrescription(prescription?.id as number, {
        comments: [...(prescription?.comments || []), comment],
      })
    }
  }

  return (
    <Dialog
      open={Boolean(isOpen)}
      onClose={handleClose}
      aria-labelledby="form-dialog-title"
    >
      <div className={classes.root}>
        <DialogTitle id="form-dialog-title">Add comment</DialogTitle>
        <DialogContent>
          <DialogContentText>
            You can add a comment to the prescription
          </DialogContentText>
          <TextField
            margin="dense"
            id="commen"
            label="Your comment"
            fullWidth
            autoFocus
            value={comment}
            onChange={(e) => setComment((e.target.value as unknown) as string)}
          />
        </DialogContent>
        <DialogActions className={classes.actions}>
          {error && <Typography color="error">Something went wrong</Typography>}
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSubmit} color="primary">
            Submit
          </Button>
        </DialogActions>
      </div>
    </Dialog>
  )
}
