import React, { useState } from 'react'
import { Theme, createStyles, makeStyles } from '@material-ui/core/styles'
import Accordion from '@material-ui/core/Accordion'
import AccordionSummary from '@material-ui/core/AccordionSummary'
import AccordionDetails from '@material-ui/core/AccordionDetails'
import Typography from '@material-ui/core/Typography'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import { Box, Container, TextField, Button, Link } from '@material-ui/core'
import { useSnackbar } from 'notistack'
import Prescription from '../../shared/Prescription'
import useStore from '../../../core/state'
import generateKey from '../../../utils/generateId'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    heroContent: {
      padding: theme.spacing(8, 0, 6),
    },
    root: {
      width: '100%',
      marginTop: '20px',
    },
    pageHeader: {
      borderBottom: 'solid 1px rgba(120, 144, 156, 0.5)',
      paddingBottom: '10px',
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      flexBasis: '33.33%',
      flexShrink: 0,
    },
    secondaryHeading: {
      fontSize: theme.typography.pxToRem(15),
      color: theme.palette.text.secondary,
    },
    container: {
      flexWrap: 'wrap',
      alignItems: 'stretch',
    },
    infoRow: {
      display: 'flex',
      minWidth: '500px',
      '& > h6': {
        flexBasis: '50%',
      },
    },
    form: {
      width: '50%',
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
    switch: {
      marginRight: 'auto',
    },
  })
)

export default function Profile() {
  const classes = useStyles()
  const { user, changePassword, updateUser, getAuthUser } = useStore(
    (state) => state
  )
  const [oldPassword, setOldPassword] = useState<string>('')
  const [newPassword, setNewPassword] = useState<string>('')
  const [confirmNewPassword, setConfirmNewPassword] = useState<string>('')
  const [error, setError] = useState<boolean>(false)
  const { enqueueSnackbar } = useSnackbar()

  async function handleChangePassword() {
    if (!error) {
      if (await changePassword(oldPassword, newPassword)) {
        setOldPassword('')
        setNewPassword('')
        setConfirmNewPassword('')
        enqueueSnackbar('Password has been changed successfully.', {
          variant: 'success',
        })
      }
    }
  }
  function handleChangeConfirmedPassword(newValue: string) {
    if (newPassword !== newValue) setError(true)
    else setError(false)

    setConfirmNewPassword(newValue)
  }
  async function handleIssueEmergencyId() {
    const emergencyId = generateKey()
    const result = await updateUser(user?.id as number, { emergencyId })
    if (result) {
      enqueueSnackbar('Emergency Id has been issued successfully', {
        variant: 'success',
      })
      getAuthUser()
    } else {
      enqueueSnackbar('An error has occured', {
        variant: 'error',
      })
    }
  }
  const isPatient = user?.roleId === 4
  return (
    <Container maxWidth="md" component="main" className={classes.heroContent}>
      <Box
        display="flex"
        alignItems="center"
        justifyContent="space-between"
        className={classes.pageHeader}
      >
        <Typography
          variant="h6"
          align="left"
          color="textPrimary"
          component="p"
          gutterBottom
        >
          Your profile
        </Typography>
      </Box>
      <div className={classes.root}>
        {isPatient && (
          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography className={classes.heading}>Prescriptions</Typography>
              <Typography className={classes.secondaryHeading}>
                View your prescriptions
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Box
                display="flex"
                alignItems="center"
                justifyContent="even"
                className={classes.container}
              >
                {user?.prescriptions.map((prescription) => (
                  <Prescription
                    prescription={prescription}
                    key={prescription.id}
                  />
                ))}
              </Box>
            </AccordionDetails>
          </Accordion>
        )}
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header"
          >
            <Typography className={classes.heading}>Personal Data</Typography>
            <Typography className={classes.secondaryHeading}>
              Your personal information
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Box>
              <Typography variant="subtitle1">
                You can see your personal information below
              </Typography>
              <br />
              <div className={classes.infoRow}>
                <Typography variant="subtitle1" color="textSecondary">
                  Full Name:
                </Typography>
                <Typography variant="subtitle1" color="textPrimary">
                  {user?.name}
                </Typography>
              </div>
              <div className={classes.infoRow}>
                <Typography variant="subtitle1" color="textSecondary">
                  Email:
                </Typography>
                <Typography variant="subtitle1" color="textPrimary">
                  {user?.email}
                </Typography>
              </div>
              <div className={classes.infoRow}>
                <Typography variant="subtitle1" color="textSecondary">
                  Gender:
                </Typography>
                <Typography variant="subtitle1" color="textPrimary">
                  {user?.gender}
                </Typography>
              </div>
              <div className={classes.infoRow}>
                <Typography variant="subtitle1" color="textSecondary">
                  Phone:
                </Typography>
                <Typography variant="subtitle1" color="textPrimary">
                  {user?.phone}
                </Typography>
              </div>
              <div className={classes.infoRow}>
                <Typography variant="subtitle1" color="textSecondary">
                  Address:
                </Typography>
                <Typography variant="subtitle1" color="textPrimary">
                  {user?.address}
                </Typography>
              </div>
              {isPatient ? (
                <div className={classes.infoRow}>
                  <Typography variant="subtitle1" color="textSecondary">
                    Emergency ID:
                  </Typography>
                  {user?.emergencyId ? (
                    <Typography variant="subtitle1" color="textPrimary">
                      {user?.emergencyId}
                    </Typography>
                  ) : (
                    <Link
                      variant="subtitle1"
                      color="primary"
                      className={classes.switch}
                      onClick={handleIssueEmergencyId}
                    >
                      Issue
                    </Link>
                  )}
                </div>
              ) : (
                false
              )}
            </Box>
          </AccordionDetails>
        </Accordion>
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel3a-content"
            id="panel3a-header"
          >
            <Typography className={classes.heading}>
              Security settings
            </Typography>
            <Typography className={classes.secondaryHeading}>
              Change your password
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <form className={classes.form} noValidate>
              <Typography variant="subtitle1">
                You can update your password here
              </Typography>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="oldPassword"
                label="Current password"
                type="password"
                id="oldPassword"
                autoComplete="current-password"
                value={oldPassword}
                onChange={(e) => setOldPassword(e.target.value)}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="newPassword"
                label="New password"
                type="password"
                id="newPassword"
                autoComplete="current-password"
                value={newPassword}
                onChange={(e) => setNewPassword(e.target.value)}
              />
              <TextField
                error={error}
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="confirmNewPassword"
                label="Confirm new password"
                type="password"
                id="confirmNewPassword"
                autoComplete="current-password"
                value={confirmNewPassword}
                onChange={(e) => handleChangeConfirmedPassword(e.target.value)}
              />
              <Button
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={handleChangePassword}
              >
                Change Password
              </Button>
            </form>
          </AccordionDetails>
        </Accordion>
      </div>
    </Container>
  )
}
