import React, { useEffect, useRef, useState } from 'react'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import {
  FormControl,
  FormControlLabel,
  InputLabel,
  makeStyles,
  MenuItem,
  Select,
  Switch,
  Typography,
  Link,
} from '@material-ui/core'
import { Link as RouterLink } from 'react-router-dom'
import { ModalType } from '../../../core/types'
import useStore from '../../../core/state'

interface ModalProps {
  isOpen: ModalType
  handleClose: () => void
  addUser: (params: any) => void
  editUser: (userId: number, params: any) => void
  error: boolean
  activeUserId: number | undefined
}

const labels = {
  addUser: {
    title: 'Add user',
    description: 'Fill out the form and click submit to add a new user.',
  },
  editUser: {
    title: 'Edit user',
    description: 'You can view or make changes to the user.',
  },
}

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3, 4, 3, 4),
  },
  actions: {
    padding: '18px 24px 8px',
  },
  select: {
    margin: '15px 0',
  },
  switch: {
    marginRight: 'auto',
  },
}))

export default function Modal({
  isOpen,
  handleClose,
  addUser,
  editUser,
  error,
  activeUserId,
}: ModalProps) {
  const classes = useStyles()
  const { users, user } = useStore((state) => state)
  const currentUser = users.find((u) => u.id === activeUserId)
  const [gender, setGender] = useState<string | undefined>()
  const [roleId, setRole] = useState<number | undefined>(currentUser?.roleId)
  const [email, setEmail] = useState<string | undefined>(currentUser?.email)
  const [password, setPassword] = useState<string | undefined>()
  const [name, setName] = useState<string | undefined>(currentUser?.name)
  const [phone, setPhone] = useState<string | undefined>(currentUser?.phone)
  const [active, setActive] = useState<boolean>(!currentUser?.deactivated)
  const [address, setAddress] = useState<string | undefined>(
    currentUser?.address
  )

  const prevState = useRef<string | null>()
  useEffect(() => {
    if (prevState.current !== isOpen && isOpen === 'addUser') {
      setGender(undefined)
      setRole(undefined)
      setEmail(undefined)
      setPassword(undefined)
      setName(undefined)
      setPhone(undefined)
      setAddress(undefined)
    }
    prevState.current = isOpen
  }, [isOpen])

  useEffect(() => {
    setGender(currentUser?.gender)
    setRole(currentUser?.roleId)
    setEmail(currentUser?.email)
    setName(currentUser?.name)
    setPhone(currentUser?.phone)
    setAddress(currentUser?.address)
  }, [
    currentUser?.address,
    currentUser?.email,
    currentUser?.gender,
    currentUser?.name,
    currentUser?.phone,
    currentUser?.roleId,
  ])

  const label: { title: string; description: string } = isOpen
    ? labels[isOpen]
    : { title: '', description: '' }

  function handleSubmit() {
    switch (isOpen) {
      case 'addUser': {
        addUser({
          gender,
          roleId,
          email,
          password,
          name,
          phone,
          address,
        })
        break
      }
      case 'editUser': {
        editUser(currentUser?.id as number, {
          gender,
          roleId,
          email,
          password,
          name,
          phone,
          address,
          deactivated: !active,
        })
        break
      }
      default:
    }
  }

  const isCreationMode = isOpen === 'addUser'
  const isAdmin = user?.roleId === 1
  const isDoctor = user?.roleId === 2

  return (
    <Dialog
      open={Boolean(isOpen)}
      onClose={handleClose}
      aria-labelledby="form-dialog-title"
    >
      <div className={classes.root}>
        <DialogTitle id="form-dialog-title">{label.title}</DialogTitle>
        <DialogContent>
          <DialogContentText>{label.description}</DialogContentText>{' '}
          <TextField
            margin="dense"
            id="name"
            label="Name"
            fullWidth
            autoFocus={Boolean(isCreationMode)}
            value={name}
            onChange={(e) => setName((e.target.value as unknown) as string)}
          />
          <TextField
            margin="dense"
            id="email"
            label="Email"
            value={email}
            onChange={(e) => setEmail((e.target.value as unknown) as string)}
            fullWidth
          />
          <FormControl fullWidth className={classes.select}>
            <InputLabel id="role-select-label">User type</InputLabel>
            <Select
              labelId="role-select-label"
              id="role-select"
              value={roleId}
              onChange={(e) => setRole((e.target.value as unknown) as number)}
              fullWidth
            >
              {isAdmin && [
                <MenuItem value={1} key="hse">
                  HSE
                </MenuItem>,
                <MenuItem value={2} key="gp">
                  GP
                </MenuItem>,
                <MenuItem value={3} key="pharma">
                  Pharmacist
                </MenuItem>,
                <MenuItem value={5} key="pharma">
                  Medical staff
                </MenuItem>,
              ]}
              {isDoctor && (
                <MenuItem value={4} key="patient">
                  Patient
                </MenuItem>
              )}
            </Select>
          </FormControl>
          {isCreationMode && (
            <TextField
              margin="dense"
              id="password"
              label="Password"
              fullWidth
              value={password}
              onChange={(e) =>
                setPassword((e.target.value as unknown) as string)
              }
            />
          )}
          <TextField
            margin="dense"
            id="phone"
            label="Phone"
            fullWidth
            value={phone}
            onChange={(e) => setPhone((e.target.value as unknown) as string)}
          />
          <FormControl fullWidth className={classes.select}>
            <InputLabel id="demo-simple-select-label">Gender</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={gender}
              onChange={(e) => setGender(e.target.value as string)}
              fullWidth
            >
              <MenuItem value="Male">Male</MenuItem>
              <MenuItem value="Female">Female</MenuItem>
              <MenuItem value="Gender neutral">Gender neutral</MenuItem>
            </Select>
          </FormControl>
          <TextField
            margin="dense"
            id="address"
            label="Address"
            fullWidth
            value={address}
            onChange={(e) => setAddress((e.target.value as unknown) as string)}
          />
        </DialogContent>
        <DialogActions className={classes.actions}>
          {isAdmin && !isCreationMode && (
            <FormControlLabel
              className={classes.switch}
              control={
                <Switch
                  checked={active}
                  onChange={() => setActive(!active)}
                  name="checkedB"
                  color="primary"
                />
              }
              label="Active"
            />
          )}
          {!isCreationMode && !isAdmin && (
            <Link
              variant="subtitle1"
              color="primary"
              component={RouterLink}
              to={`/user/${currentUser?.id}/prescriptions`}
              className={classes.switch}
            >
              View prescriptions
            </Link>
          )}
          {error && <Typography color="error">Something went wrong</Typography>}
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSubmit} color="primary">
            Submit
          </Button>
        </DialogActions>
      </div>
    </Dialog>
  )
}
