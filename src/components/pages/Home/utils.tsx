export function getHomePageTitle(roleId: number) {
  let title: string
  switch (roleId) {
    case 1: {
      title = 'Medical staff'
      break
    }
    case 2: {
      title = 'Your patients'
      break
    }
    case 3: {
      title = 'Patients'
      break
    }
    case 4:
    default: {
      title = 'Users'
      break
    }
  }
  return title
}
