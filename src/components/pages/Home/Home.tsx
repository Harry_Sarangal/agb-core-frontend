/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { ChangeEvent, useEffect, useState } from 'react'
import Typography from '@material-ui/core/Typography'
import { makeStyles, fade } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'
import { VariantType, useSnackbar } from 'notistack'
import { Box, Button } from '@material-ui/core'
import SearchIcon from '@material-ui/icons/Search'
import InputBase from '@material-ui/core/InputBase'
import { useHistory } from 'react-router-dom'
import StickyHeadTable from './Table'
import useStore from '../../../core/state'
import { ModalType } from '../../../core/types'
import Modal from './Modal'
import { getHomePageTitle } from './utils'

const useStyles = makeStyles((theme) => ({
  heroContent: {
    padding: theme.spacing(8, 0, 6),
  },
  pageHeader: {
    borderBottom: 'solid 1px rgba(120, 144, 156, 0.5)',
    paddingBottom: '10px',
  },
  title: {
    margin: 0,
  },
  button: {
    marginLeft: 'auto',
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.black, 0.1),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.black, 0.05),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
}))

export default function Home() {
  const classes = useStyles()
  const { users, fetchUsers, addUser, user, updateUser } = useStore(
    (state) => state
  )
  const [isOpen, setIsOpen] = useState<ModalType>(null)
  const [error, setError] = useState<boolean>(false)
  const [activeUserId, setActiveUserId] = useState<number>()
  const [searchQuery, setSearchQuery] = useState<string>('')
  const { enqueueSnackbar } = useSnackbar()
  const history = useHistory()

  useEffect(() => {
    fetchUsers()
  }, [fetchUsers])

  const isPatient = user?.roleId === 4
  if (isPatient) {
    history.push('/profile')
  }

  function handleClose() {
    setIsOpen(null)
    setActiveUserId(undefined)
    setError(false)
  }

  const handleClickVariant = (variant: VariantType, message: string) => {
    // variant could be success, error, warning, info, or default
    enqueueSnackbar(message, {
      variant,
    })
  }

  function handeAddUser(params: any) {
    addUser(params).then((v) => {
      if (v) {
        fetchUsers()
        handleClose()
        handleClickVariant('success', 'Success!')
      } else {
        setError(true)
      }
    })
  }

  function handeEditUser(userId: number, params: any) {
    updateUser(userId, params).then((v) => {
      if (v) {
        fetchUsers()
        handleClose()
        handleClickVariant('success', 'User has been updated!')
      } else {
        setError(true)
      }
    })
  }

  function handleSearch(e: ChangeEvent<HTMLInputElement>) {
    setSearchQuery(e.target.value)
  }

  const filteredUsers = users.filter((u) => {
    const nameMatch = (u.name || '')
      .toLocaleLowerCase()
      .includes(searchQuery.toLowerCase())
    const emailMatch = (u.email || '')
      .toLocaleLowerCase()
      .includes(searchQuery.toLowerCase())
    const phoneMatch = (u.phone || '')
      .toLocaleLowerCase()
      .includes(searchQuery.toLowerCase())
    return nameMatch || emailMatch || phoneMatch
  })

  const isAdmin = user?.roleId === 1
  const isDoctor = user?.roleId === 2
  const isPharmacist = user?.roleId === 3
  const isStaff = user?.roleId === 5

  const title = getHomePageTitle(user?.roleId as number)

  return (
    <Container maxWidth="md" component="main" className={classes.heroContent}>
      <Box display="flex" alignItems="center" className={classes.pageHeader}>
        <Typography
          variant="h6"
          align="left"
          color="textPrimary"
          component="p"
          gutterBottom
          className={classes.title}
        >
          {title}
        </Typography>
        <div className={classes.search}>
          <div className={classes.searchIcon}>
            <SearchIcon />
          </div>
          <InputBase
            placeholder="Search…"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            inputProps={{ 'aria-label': 'search' }}
            onChange={handleSearch}
          />
        </div>
        {(isAdmin || isDoctor) && (
          <Button
            variant="outlined"
            color="secondary"
            onClick={() => setIsOpen('addUser')}
            className={classes.button}
          >
            {isAdmin ? 'Add Employee' : 'Add Patient'}
          </Button>
        )}
      </Box>
      <StickyHeadTable
        users={filteredUsers}
        handleViewUser={(id) => {
          if (id && (isAdmin || isDoctor)) {
            setIsOpen('editUser')
            setActiveUserId(id)
          }
          if (id && (isPharmacist || isStaff)) {
            const viewedUser = users.find((u) => u.id === id)
            if (viewedUser?.roleId === 4) {
              history.push(`/user/${id}/prescriptions`)
            }
          }
        }}
      />
      {isOpen && (
        <Modal
          isOpen={isOpen}
          handleClose={handleClose}
          addUser={handeAddUser}
          editUser={handeEditUser}
          error={error}
          activeUserId={activeUserId}
        />
      )}
    </Container>
  )
}
