import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TablePagination from '@material-ui/core/TablePagination'
import TableRow from '@material-ui/core/TableRow'
import { Typography } from '@material-ui/core'
import { Roles, User } from '../../../core/types'

interface Column {
  id: 'name' | 'email' | 'roleId' | 'gender' | 'phone'
  label: string
  minWidth?: number
  align?: 'right'
  format?: (value: number) => string
}

const columns: Column[] = [
  { id: 'name', label: 'Name', minWidth: 170 },
  {
    id: 'roleId',
    label: 'Type',
    minWidth: 120,
  },
  { id: 'email', label: 'Email', minWidth: 100 },
  {
    id: 'gender',
    label: 'Gender',
    align: 'right',
    minWidth: 100,
  },
  {
    id: 'phone',
    label: 'Phone',
    align: 'right',
    minWidth: 150,
  },
]

const useStyles = makeStyles({
  root: {
    width: '100%',
    marginTop: '20px',
  },
  container: {
    maxHeight: 587,
  },
  row: {
    cursor: 'pointer',
  },
  rowDisabled: {
    cursor: 'pointer',
    opacity: 0.3,
  },
  emptyMessage: {
    margin: '2em 0',
  },
})

interface TableProps {
  users: User[]
  handleViewUser: (id: number) => void
}

export default function TableComponent({ users, handleViewUser }: TableProps) {
  const classes = useStyles()
  const [page, setPage] = React.useState(0)
  const [rowsPerPage, setRowsPerPage] = React.useState(10)

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage)
  }

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(+event.target.value)
    setPage(0)
  }

  return (
    <Paper className={classes.root}>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {users.length === 0 && (
              <TableRow>
                <TableCell colSpan={5}>
                  <Typography
                    variant="h6"
                    align="center"
                    color="textSecondary"
                    component="p"
                    className={classes.emptyMessage}
                  >
                    Empty
                  </Typography>
                </TableCell>
              </TableRow>
            )}
            {users
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((user: User) => {
                return (
                  <TableRow
                    hover
                    tabIndex={-1}
                    key={user.id}
                    className={
                      user.deactivated ? classes.rowDisabled : classes.row
                    }
                    onClick={() => handleViewUser(user.id)}
                  >
                    {columns.map((column) => {
                      let value = user[column.id]
                      if (column.id === 'roleId') {
                        value = Roles[((value as unknown) as number) - 1]
                      }
                      return (
                        <TableCell key={column.id} align={column.align}>
                          {column.format && typeof value === 'number'
                            ? column.format(value)
                            : value}
                        </TableCell>
                      )
                    })}
                  </TableRow>
                )
              })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={users.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
  )
}
