import { Typography } from '@material-ui/core'
import React from 'react'
import img from '../../logo500.500.png'

export default function Logo() {
  return (
    <>
      <img src={img} alt="" style={{ width: '2em', marginRight: '1em' }} />
      <Typography variant="h5" color="primary" noWrap>
        Dr. Daycare
      </Typography>
    </>
  )
}
