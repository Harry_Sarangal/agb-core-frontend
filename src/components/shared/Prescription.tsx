import React, { useRef } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import PrintIcon from '@material-ui/icons/Print'
import LaunchIcon from '@material-ui/icons/Launch'
import ChatBubbleOutlineIcon from '@material-ui/icons/ChatBubbleOutline'
import Typography from '@material-ui/core/Typography'
import { format } from 'date-fns'
import { Prescription as PrescriptionType } from '../../core/types'
import print from '../../utils/print/print'

const useStyles = makeStyles({
  root: {
    minWidth: 285,
    maxWidth: 285,
    margin: 10,
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: 'rgba(0, 0, 0, 0.03)',
    },
  },
  actions: {
    marginTop: 'auto',
    padding: '16px',
  },
  icon: {
    cursor: 'pointer',
    color: '#78909c',
    '&:hover': {
      color: '#2196f3',
    },
  },
  commentBlock: {
    display: 'flex',
    alignItems: 'center',
    color: '#aaaaaa',
    marginLeft: 'auto !important',
  },
  commentIcon: {
    width: '0.7em',
    height: '0.7em',
  },
  commentNumber: {
    fontSize: '1.1em',
    fontWeight: 'bold',
    marginRight: '4px',
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
})

interface PrescriptionProps {
  prescription: PrescriptionType
  // eslint-disable-next-line react/require-default-props
  handleOnClick?: () => void
  // eslint-disable-next-line react/require-default-props
  isPrintable?: boolean
  // eslint-disable-next-line react/require-default-props
  handleViewClick?: () => void
}

export default function Prescription({
  prescription,
  handleOnClick,
  handleViewClick,
  isPrintable = false,
}: PrescriptionProps) {
  const classes = useStyles()
  const ref = useRef(null)

  const numberOfComments = prescription?.comments?.length || 0

  return (
    <Card className={classes.root} variant="outlined">
      <CardContent className={classes.content} onClick={handleOnClick}>
        <div ref={ref}>
          <Typography
            className={classes.title}
            color="textSecondary"
            gutterBottom
          >
            {format(new Date(prescription.createdAt), 'd MMM yyyy')}
          </Typography>
          <Typography variant="h5" component="h2">
            {prescription.name}
          </Typography>
          <Typography className={classes.pos} color="textSecondary">
            Refils: {prescription.refils}
          </Typography>
          <Typography variant="body2" component="p">
            {prescription.information}
            <br />
          </Typography>
        </div>
      </CardContent>
      <CardActions className={classes.actions}>
        {isPrintable && (
          <PrintIcon
            className={classes.icon}
            onClick={() => print([ref.current])}
          />
        )}
        {handleViewClick && (
          <LaunchIcon className={classes.icon} onClick={handleViewClick} />
        )}
        {Boolean(numberOfComments && handleViewClick) && (
          <div className={classes.commentBlock}>
            <span className={classes.commentNumber}>{numberOfComments}</span>
            <ChatBubbleOutlineIcon className={classes.commentIcon} />
          </div>
        )}
      </CardActions>
    </Card>
  )
}
