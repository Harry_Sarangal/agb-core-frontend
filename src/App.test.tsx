import React from 'react'
import { render, screen, waitFor } from '@testing-library/react'
import App from './App'

test('renders login window', async () => {
  render(<App />)
  let linkElement
  await waitFor(() => {
    linkElement = screen.getByText(/Sign in to Dr. Daycare/i)
  })
  expect(linkElement).toBeInTheDocument()
})
