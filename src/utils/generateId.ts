import crypto from 'crypto'

const generateKey = () => {
  // 16 bytes is likely to be more than enough,
  // but you may tweak it to your needs
  return crypto.randomBytes(8).toString('base64')
}

export default generateKey
