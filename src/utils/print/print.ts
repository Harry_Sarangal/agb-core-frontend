/* eslint-disable no-plusplus */
/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/naming-convention */

const hideFromPrintClass = 'pe-no-print'
const preservePrintClass = 'pe-preserve-print'
const preserveAncestorClass = 'pe-preserve-ancestor'
const bodyElementName = 'BODY'

const _hide = function (element: {
  classList: { contains: (arg0: string) => any; add: (arg0: string) => void }
}) {
  if (!element.classList.contains(preservePrintClass)) {
    element.classList.add(hideFromPrintClass)
  }
}

const _preserve = function (
  element: {
    classList: { remove: (arg0: string) => void; add: (arg0: string) => void }
  },
  isStartingElement: any
) {
  element.classList.remove(hideFromPrintClass)
  element.classList.add(preservePrintClass)
  if (!isStartingElement) {
    element.classList.add(preserveAncestorClass)
  }
}

const _clean = function (element: {
  classList: { remove: (arg0: string) => void }
}) {
  element.classList.remove(hideFromPrintClass)
  element.classList.remove(preservePrintClass)
  element.classList.remove(preserveAncestorClass)
}

const _walkSiblings = function (
  element: { previousElementSibling: any; nextElementSibling: any },
  callback: { (element: any): void; (element: any): void; (arg0: any): void }
) {
  let sibling = element.previousElementSibling
  while (sibling) {
    callback(sibling)
    sibling = sibling.previousElementSibling
  }
  sibling = element.nextElementSibling
  while (sibling) {
    callback(sibling)
    sibling = sibling.nextElementSibling
  }
}

const _attachPrintClasses = function (element: any, isStartingElement: any) {
  _preserve(element, isStartingElement)
  _walkSiblings(element, _hide)
}

const _cleanup = function (element: any, isStartingElement: any) {
  _clean(element)
  _walkSiblings(element, _clean)
}

const _walkTree = function (
  element: any,
  callback: {
    (element: any, isStartingElement: any): void
    (element: any, isStartingElement: any): void
    (arg0: any, arg1: boolean): void
  }
) {
  let currentElement = element
  callback(currentElement, true)
  currentElement = currentElement.parentElement
  while (currentElement && currentElement.nodeName !== bodyElementName) {
    callback(currentElement, false)
    currentElement = currentElement.parentElement
  }
}

const _print = function (elements: string | any[]) {
  for (let i = 0; i < elements.length; i++) {
    _walkTree(elements[i], _attachPrintClasses)
  }
  window.print()
  for (let i = 0; i < elements.length; i++) {
    _walkTree(elements[i], _cleanup)
  }
}

export default _print
