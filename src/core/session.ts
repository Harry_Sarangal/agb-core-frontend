const key = 'sessionId'

export function setSessionId(sessionId: string): void {
  localStorage.setItem(key, sessionId)
}

export function getSessionId(): string | null {
  return localStorage.getItem(key)
}

export function removeSessionId(): void {
  localStorage.removeItem(key)
}
