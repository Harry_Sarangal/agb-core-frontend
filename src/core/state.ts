import create from 'zustand'
import { getSessionId, removeSessionId, setSessionId } from './session'
import { User } from './types'

type AppState = {
  user: User | null
  loading: boolean
  users: User[]
  login: (email: string, password: string) => void
  error: boolean
  logout: () => void
  fetchUsers: () => void
  fetchUser: (id: number) => Promise<User | null>
  getAuthUser: () => void
  addUser: (params: any) => Promise<boolean>
  updateUser: (userId: number, params: any) => Promise<boolean>
  changePassword: (oldPassword: string, newPassword: string) => Promise<boolean>
  addPrescription: (params: any) => Promise<boolean>
  updatePrescription: (prescriptionId: number, params: any) => Promise<boolean>
}

const api = 'http://134.209.82.240:5000/api'
// const api = 'http://localhost:5000/api'
const requestHeaders = { 'Content-Type': 'application/json' }

const useStore = create<AppState>((set) => ({
  user: null,
  error: false,
  loading: true,
  users: [],
  fetchUsers: async () => {
    const headers = { session_id: getSessionId() || '' }
    const response = await fetch(`${api}/users`, { method: 'GET', headers })
    const data = await response.json()
    if (Array.isArray(data)) {
      set({ users: data })
    }
  },
  fetchUser: async (id: number) => {
    const headers = { session_id: getSessionId() || '' }
    const response = await fetch(`${api}/users/${id}`, {
      method: 'GET',
      headers,
    })
    const user = (await response.json()) as User
    if (user && user.id) {
      return user
    }
    return null
  },
  login: async (email, password) => {
    const body = {
      email,
      password,
    }

    const response = await fetch(`${api}/login`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: requestHeaders,
    })

    const user = await response.json()

    if (user && !user.error && user.id) {
      setSessionId(user.sessionId)
      set({ user, error: false })
    } else {
      set({ error: true })
    }
  },
  logout: async () => {
    const body = {
      sessionId: getSessionId(),
    }
    await fetch(`${api}/logout`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: requestHeaders,
    })
    removeSessionId()
    set({ user: null })
  },
  getAuthUser: async () => {
    const body = {
      sessionId: getSessionId(),
    }
    const response = await fetch(`${api}/me`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: requestHeaders,
    })
    const user = await response.json()
    if (user && !user.error && user.id) {
      set({ user })
    }
    set({ loading: false })
  },
  addUser: async (params) => {
    const body = {
      sessionId: getSessionId(),
      ...params,
    }
    const response = await fetch(`${api}/users`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: requestHeaders,
    })
    const user = await response.json()
    if (user && !user.errors && user.id) {
      return true
    }
    return false
  },
  updateUser: async (userId, params) => {
    const body = {
      sessionId: getSessionId(),
      ...params,
    }
    const headers = { session_id: getSessionId() || '' }
    const response = await fetch(`${api}/users/${userId}`, {
      method: 'PUT',
      body: JSON.stringify(body),
      headers: { ...requestHeaders, ...headers },
    })
    const user = await response.json()
    if (user && !user.errors && user.id) {
      return true
    }
    return false
  },
  changePassword: async (oldPassword, newPassword) => {
    const body = {
      oldPassword,
      newPassword,
      sessionId: getSessionId(),
    }

    const response = await fetch(`${api}/password`, {
      method: 'PUT',
      body: JSON.stringify(body),
      headers: requestHeaders,
    })

    const user = await response.json()

    if (user && !user.error && user.id) {
      return true
    }
    return false
  },
  addPrescription: async (params) => {
    const body = {
      sessionId: getSessionId(),
      ...params,
    }
    const response = await fetch(`${api}/prescriptions`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: requestHeaders,
    })
    const prescription = await response.json()
    if (prescription && !prescription.errors && prescription.id) {
      return true
    }
    return false
  },
  updatePrescription: async (id: number, params) => {
    const body = {
      sessionId: getSessionId(),
      ...params,
    }
    const headers = { session_id: getSessionId() || '' }
    const response = await fetch(`${api}/prescriptions/${id}`, {
      method: 'PUT',
      body: JSON.stringify(body),
      headers: { ...requestHeaders, ...headers },
    })
    const prescription = await response.json()
    if (prescription && !prescription.errors && prescription.id) {
      return true
    }
    return false
  },
}))

export default useStore
