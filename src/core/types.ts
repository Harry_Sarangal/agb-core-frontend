export interface User {
  id: number
  name: string
  email: string
  roleId: number
  gender: string
  phone: string
  address: string
  deactivated: boolean
  prescriptions: Prescription[]
  allergies: string[]
  emergencyId: string
}

export enum Roles {
  'HSE',
  'GP',
  'Pharmacist',
  'Patient',
  'Medical staff',
}

export type ModalType = 'addUser' | 'editUser' | null
export type PrescriptionModalType =
  | 'addPrescription'
  | 'updatePrescription'
  | null

export interface Prescription {
  id: number
  name: string
  refils: number
  information: string
  createdAt: string
  comments: string[]
}
