# Contribution guide

## Branching

Branch prefix for features — **feature/**

Branch prefix for fixes — **fix/**

On taking a new task and moving it to IN-DEV status, you must create a new branch from a master branch and name it according to the following template:

`[prefix]agb_[task_id]_simple_description`

Example:

`feature/100_navbar_component`

## Commiting

Under any circumstance, you must not commit to the main branch (master) directly.

When commiting you must adhere to the followiing rules:

- Prefix you commit message with a task number like that AGB-100

- Then usually follows one of imperative verbs, i.e Add/Imrove/Implement/Adjust

- Finally, a short message reflecting what you've done, i.e Modal Popup/Navbar component etc.

Example:

`AGB-100 Add navbar component`

## Pull requests and code review

After you are done with your task, you need to open a new Pull Request (PR).

PR naming should reflect the main point of the task (Generally, you can copy paste task name)

Example:

`AGB-100 Navbar component`

We will be merging PRs after they undergo at least 1 (but ideally 2-3) code review from a team member. 

---

Everything above is aimed to organize a beautiful, uniform, and healthy git workflow, but obviously, is not a matter of utmost importance to our very existence, especially considering that this is a school project, so it's okay if you don't get it perfect every time.

In case of any questions ping @Sergey
