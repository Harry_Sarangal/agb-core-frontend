# agb-core-frontend

### Getting started

To be able to use following scripts you have to have `nodejs` and `npm` installed in your system.

1. After cloning the project run `npm install`
2. Start project locally `npm start`
3. You are all set up

### Available Scripts

**`npm start`**

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

**`npm test`**

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

**`npm build`**

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.
